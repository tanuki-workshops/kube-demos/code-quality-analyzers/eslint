# Code Quality ESLint for GitLab CI

## Example

```yaml
stages:
  - test

try:eslint:helper:
  stage: test
  image: registry.gitlab.com/tanuki-workshops/kube-demos/code-quality-analyzers/eslint:latest
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
  script:
    - analyze ./
```

**Or:**

```yaml
stages:
  - 🔎code-quality
  
include:
  - project: 'tanuki-workshops/kube-demos/code-quality-analyzers/eslint'
    file: 'eslint.gitlab-ci.yml'

🔎:code:quality:eslint:
  stage: 🔎code-quality
  extends: .eslint:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  script:
    - analyze ./
```

> See: https://gitlab.com/tanuki-helpers/try-eslint-helper

