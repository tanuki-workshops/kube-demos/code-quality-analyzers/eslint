#!/usr/bin/env node

const fs = require("fs")
const { exec } = require("child_process")

let cmdArgs = process.argv.slice(2);
let directory = cmdArgs[0]

//let cmd = `eslint --config .eslintrc.yml --no-eslintrc --ext ".html,.js" --format json --output-file report.json **/*.js`
let cmd = `eslint --config .eslintrc.yml --no-eslintrc --ext ".html,.js" --format json --output-file report.json ${directory}/**/*.js`

exec(cmd, (error, stdout, stderr) => {

    if (error) {
        console.log(`😡 error(s) found: ${error.message}`)
        try {
            let content = fs.readFileSync("./report.json")

            /* --- helpers --- */
            let btoa = (string) => Buffer.from(string).toString("base64")
            let fingerprint = (description, path, line, column) => `${btoa(description)}-${btoa(path)}-${btoa(line)}-${btoa(column)}`

            let eslintResults = JSON.parse(content).flatMap(item => {
                return item.messages.map(message => {
                    //console.log("👋", process.env.CI_PROJECT_DIR)
                    message.filePath = item.filePath.replace(process.env.CI_PROJECT_DIR,"")
                    return message
                })
            })

            console.log("📝 eslint report:")
            console.log(`------------------------------------------------------------`)
            eslintResults.forEach(item => {
            console.log(` 🔴 Rule: ${item.ruleId}: ${item.message}`)
            console.log(`    file: ${item.filePath}`)
            console.log(`    line: ${item.line}`)
            console.log(`------------------------------------------------------------`)
            })

            /* --- generate gl-code-quality-report.json --- */
            let codeQualityData = eslintResults.map(item => {
                let fp = fingerprint(item.message, item.filePath, item.line.toString(), item.column.toString())
                return {
                    description: `Rule: ${item.ruleId}: ${item.message}`,
                    fingerprint: fp,
                    location: {
                    path: item.filePath,
                        lines: {
                            begin: item.line
                        }
                    }
                }
            })

            fs.writeFileSync("./gl-code-quality-report.json", JSON.stringify(codeQualityData, null, 2))

            return
        } catch (error) {
          // no report.json file
          fs.writeFileSync("./gl-code-quality-report.json", "[]")
        }

    } else {
        fs.writeFileSync("./gl-code-quality-report.json", "[]")
    }

    if (stderr) {
        console.log(`stderr: ${stderr}`)
        return
    }
    console.log(`stdout: ${stdout}`)
})



