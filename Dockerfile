# nodejs helpers 2020-09-30 by @k33g | on gitlab.com 
FROM node:14-alpine
LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

RUN npm install -g eslint

COPY analyze.js /usr/local/bin/analyze

RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]
